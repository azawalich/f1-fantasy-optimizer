from datetime import datetime
import time
import argparse
import glob
from utils.download_data import *
from utils.save_data import *
from utils.get_teams import *
from utils.get_players_prices import *
from utils.get_players_streaks import * 
from utils.create_combinations import *
from utils.upload_files import *
from utils.commit_files import *
from utils.get_and_download_current_season_data import *
from utils.create_leagues_df import *
import pandas as pd
pd.options.mode.chained_assignment = None

parser = argparse.ArgumentParser()
parser.add_argument("--country", help="Race week country name", type=str)
parser.add_argument("--type", help="Executed session type", type=str)
parser.add_argument("--budget", help="Budget to do combinations with", type=float)
parser.add_argument("--commit", help="Commit code or not", type=str)
parser.add_argument("--current_season", help="Run simulations for current season", type=str)

args = parser.parse_args()
if not args.budget:
    budget = 100.0
else:
    budget = args.budget

if not args.commit:
    commit = None
else:
    commit = args.commit

if not args.current_season:
    current_season = None
else:
    current_season = args.current_season

endpoints = [
    # full information about drivers/teams
    'https://fantasy-api.formula1.com/partner_games/f1/players',
    # players' current teams
    'https://fantasy-api.formula1.com/partner_games/f1/picked_teams?my_current_picked_teams=true'
]

folders_list = [
    'data/raw',
    'data/raw',
    'data/files',
    'data/combinations'
] 

current_teams = None
players_prices = None
current_time = str(datetime.now())
current_time_format = current_time.replace(' ', '_').split('.')[0]
old_folder = sorted(glob.glob('data/combinations/*'))[-1]

folder_paths = create_folders(folders_list, current_time, args.country, args.type)

endpoints_tuple = list(zip(endpoints, folder_paths))

for single_tuple in endpoints_tuple:
    single_endpoint, single_path = single_tuple
    endpoint_name = single_endpoint.split('/')[-1].split('?')[0]
    response = download_data(single_endpoint)
    data_text = save_data(response, endpoint_name, single_path)
    time.sleep(1)
    if 'players' in single_endpoint:
        players_prices = get_players_prices(json.loads(data_text), current_season)
        players_streaks = get_players_streaks(json.loads(data_text))
    if 'picked_teams' in single_endpoint:
        current_teams = get_teams(json.loads(data_text), current_season, players_prices)

combinations_df_list, dc_file_path = create_combinations_full_df(folder_paths[0], folder_paths[-2], current_teams, players_prices, players_streaks, current_season, args.type)

folder_names_full = []
files_paths_full = []
dummy_folder_name = None

for desired_indeks in range(0, len(combinations_df_list)):

    if current_season is not None:
        single_combination_df = combinations_df_list[desired_indeks]
        budget = current_teams[desired_indeks]['team_name'].replace(' ', '_')

    folder_names, files_paths = sort_and_save_combinations_full_df(single_combination_df, folder_paths[-1], old_folder, budget, args.type)

    if desired_indeks == len(combinations_df_list)-1:
        # make dummy folder/file names for drivers/constructors stuff
        dummy_folder_name = folder_names[0]
        folder_names.append(dummy_folder_name)
        files_paths.append(dc_file_path)
    
    folder_names_full += folder_names
    files_paths_full += files_paths

if args.type == 'R':
    get_and_download_current_season_data('2021')
    leagues_filepath = create_leagues_df(current_time_format, args.country, args.type)
    create_leagues_joined_df('data/leagues', 'files/leagues_results.csv')
    folder_names_full += [dummy_folder_name, 'files', 'files', 'files']
    files_paths_full += [leagues_filepath, 'files/leagues_results.csv', 'files/drivers.csv', 'files/constructors.csv']
    folder_paths = folder_paths + [
        'data/historical/2021',
        'data/leagues'
    ]

google_drive_upload_wrapper(folder_names_full, files_paths_full)

if commit is not None:
    commit_message = 'add data for {} {}'.format(args.country, args.type)
    commit_files_list = [s + '/*' for s in folder_paths[1:]] + ['files/execution.csv']
    if args.type == 'R':
        commit_files_list = commit_files_list + ['files/leagues_results.csv', 'files/drivers.csv', 'files/constructors.csv']
    git_push(commit_message, commit_files_list)
