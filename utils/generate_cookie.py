import requests
import json
import ast
import base64
import utils.send_email as se

def generate_cookie(username, password):
    proper_cookie_decoded = None
    url = "https://api.formula1.com/6657193977244c13?d=account.formula1.com"
    payload = {
        "solution": {
            "interrogation": {
                    "st": 162229509,
                    "sr": 1959639815,
                    "cr": 78830557
                },
            "version": "stable"
            },
        "error": None,
        "performance": {
            "interrogation":185
        }
    }
    headers = {}

    response = requests.post(url, headers=headers, data=json.dumps(payload))

    if response.status_code == 200:
        response_dict = ast.literal_eval(response.text)
        
        if isinstance(response_dict, dict):
            url = "https://api.formula1.com/v2/account/subscriber/authenticate/by-password"

            payload = {
                "DistributionChannel": "d861e38f-05ea-4063-8776-a7e2b6d885a4",
                "Login": username,
                "Password": password
            }

            cookie_payload = '; login={\"event\":\"login\",\"componentId\":\"component_login_page\",\"actionType\":\"failed\"}'

            headers = {
                'apiKey': 'fCUCjWrKPu9ylJwRAv8BpGLEgiAuThx7',
                'Cookie': 'reese84={}{}'.format(response_dict['token'], cookie_payload)
            }

            response = requests.post(url, headers=headers, data=json.dumps(payload))

            if response.status_code == 200:

                cookie_data = json.loads(response.text)
                
                proper_cookie_payload = json.dumps({
                    'data': {
                        'subscriptionToken': cookie_data['data']['subscriptionToken']
                    }
                })

                proper_cookie_decoded = base64.b64encode(proper_cookie_payload.encode()).decode()
                
            else:
                se.send_email(
                    subject = 'Cookie update failed!',
                    content = 'Cookie update failed! Second auth status Code: {} Status text: {}'.format(
                        response.status_code, response.text
                    )
                )
        else:
            se.send_email(
                subject = 'Cookie update failed!',
                content = 'Cookie update failed! Reason: First auth response of wrong type. Type: {}. Object: {}'.format(
                    type(response_dict), str(response_dict)
                )
            )
    else:
        se.send_email(
        subject = 'Cookie update failed!',
        content = 'Cookie update failed! First auth status Code: {} Status text: {}'.format(
            response.status_code, response.text
            )
        )
    return proper_cookie_decoded