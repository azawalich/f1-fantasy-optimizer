from utils.drivers_api import *
def get_players_prices(players_data, current_season):
    players_prices = {}
    for single_player in players_data['players']:
        player_name = drivers_api[single_player['id']]
        if current_season is None:
            if single_player['weekly_price_change'] == single_player['price']:
                change = 0.0
            else:
                change = single_player['weekly_price_change']
            players_prices[player_name] = change
        else:
            players_prices[player_name] = single_player['price']
    return players_prices