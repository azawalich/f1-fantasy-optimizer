from utils.download_data import *
from utils.save_data import *
import time
import pandas as pd
import glob
import numpy as np
from tqdm import tqdm

def create_leagues_df(current_time, race_name, race_type):

    endpoint = 'https://fantasy-api.formula1.com/partner_games/f1/league_entrants'
    league_data = download_data(endpoint)
    save_data(league_data, 'league_entrants', 'data/raw/{}_{}_{}'.format(current_time, race_name, race_type))

    leagues_dict = {
        'name': [],
        'id': [],
        'entrants_count': [],
        'picked_team_id': [],
        'picked_team_name': []
    }

    for single_league in json.loads(league_data.text)['league_entrants']:
        leagues_dict['name'].append(single_league['league']['name'])
        leagues_dict['id'].append(single_league['league']['id'])
        leagues_dict['entrants_count'].append(single_league['league']['entrants_count'])
        leagues_dict['picked_team_id'].append(single_league['picked_team_id'])
        leagues_dict['picked_team_name'].append(single_league['picked_team_name'])

    leagues_details_dict = {
        'id': [],
        'picked_team_name': [],
        'team_rank': [],
        'team_score': [],
        'score_to_champion': []
    }

    print('Downloading detailed league data...')
    for single_id in tqdm(leagues_dict['id']):
        endpoint = 'https://fantasy-api.formula1.com/partner_games/f1/leaderboards/leagues?league_id={}'.format(single_id)
        league_detailed_data = download_data(endpoint)
        save_data(league_detailed_data, 'leagues', 'data/raw/{}_{}_{}'.format(current_time, race_name, race_type))
        league_detailed_data_object = json.loads(league_detailed_data.text)['leaderboard']
        my_teams = league_detailed_data_object['my_positions']
        champion_score = league_detailed_data_object['leaderboard_entrants'][0]['score']
        for single_key in my_teams.keys():
            leagues_details_dict['id'].append(single_id)
            leagues_details_dict['picked_team_name'].append(my_teams[single_key]['team_name'])
            leagues_details_dict['team_rank'].append(my_teams[single_key]['rank'])
            leagues_details_dict['team_score'].append(my_teams[single_key]['score'])
            leagues_details_dict['score_to_champion'].append(champion_score - my_teams[single_key]['score'])
        time.sleep(1)

    leagues_dict_df = pd.DataFrame.from_dict(leagues_dict).sort_values(['id', 'picked_team_id'])
    leagues_dict_df['entrants_count_corrrected'] = 0
    for single_league in leagues_dict_df['name'].drop_duplicates():
        league_teams = leagues_dict_df[leagues_dict_df['name'] == single_league].shape[0]
        entrants_count_new = leagues_dict_df[leagues_dict_df['name'] == single_league]['entrants_count'].drop_duplicates().tolist()[0] * league_teams
        leagues_dict_df.loc[leagues_dict_df['name'] == single_league, 'entrants_count_corrrected'] = entrants_count_new

    leagues_details_dict_df = pd.DataFrame.from_dict(leagues_details_dict)

    leagues_df_joined = pd.merge(
        leagues_dict_df,
        leagues_details_dict_df,
        how='left',
        on=['id', 'picked_team_name']
    )

    leagues_df_joined = leagues_df_joined[leagues_df_joined['team_score'] > 0]

    leagues_df_joined['team_rank_percentile_below'] = round(
        1 - (leagues_df_joined['team_rank'] / leagues_df_joined['entrants_count_corrrected']), 6
        )

    leagues_df_joined = leagues_df_joined.drop(columns=['entrants_count'])

    filepath = 'data/leagues/{}_{}.csv'.format(current_time, race_name)

    leagues_df_joined.drop_duplicates().to_csv(filepath)

    return filepath

def create_leagues_joined_df(folder_path, filepath):
    files = sorted(glob.glob('{}/*.csv'.format(folder_path))[::-1])

    for single_file in files:
        indeks = files.index(single_file)
        race = '{}_{}'.format(
            indeks+1,
            single_file.split('_')[-1].replace('.csv', '')
        )
        temp_df = pd.read_csv(single_file)
        temp_df['race'] = race
        if indeks == 0:
            leagues_df_joined = temp_df
        else:
            leagues_df_joined = leagues_df_joined.append(temp_df)

    leagues_df_joined = leagues_df_joined.loc[:,~leagues_df_joined.columns.str.startswith('Unnamed')].sort_values(['id', 'picked_team_id', 'race']).drop_duplicates()

    grouped_df = leagues_df_joined.sort_values(['id', 'picked_team_id', 'race']).groupby(['id', 'picked_team_id', 'race'])
    columns_to_diff = ['entrants_count_corrrected', 'team_rank', 'team_score', 'team_rank_percentile_below', 'score_to_champion']
    for single_column in columns_to_diff:
        new_column = '{}_diff'.format(single_column)
        leagues_df_joined[new_column] = grouped_df[single_column].apply(pd.to_numeric).diff().fillna(0)

    firsts = leagues_df_joined[leagues_df_joined['race'] == leagues_df_joined['race'].drop_duplicates().tolist()[0]]
    seconds = leagues_df_joined[leagues_df_joined['race'] != leagues_df_joined['race'].drop_duplicates().tolist()[0]]

    for single_column in columns_to_diff:
        new_column = '{}_diff'.format(single_column)
        firsts[new_column] = np.nan
        if new_column == 'team_rank_diff':
            seconds[new_column] = seconds[new_column] * -1

    leagues_df_joined_final = firsts.append(seconds)

    leagues_df_joined_final['race_number'] = leagues_df_joined_final['race'].str.split('_').str[0]

    leagues_df_joined_final = leagues_df_joined_final.sort_values(['id', 'picked_team_name', 'race_number']).drop(columns=['race_number'])

    leagues_df_joined_final.to_csv(filepath)

    print('Saved global leagues file to: {}'.format(filepath))
