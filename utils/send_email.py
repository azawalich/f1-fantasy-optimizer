import smtplib
from email.message import EmailMessage
import utils.initiate_secrets as ins
import datetime

secrets = ins.initiate_secrets()
current_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

def send_email(subject, content):
    msg = EmailMessage()
    msg.set_content(content)
    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = subject
    msg['From'] = secrets['mail_login']
    msg['To'] = secrets['mail_login2']

    # Send the message via our own SMTP server.
    s = smtplib.SMTP(secrets['mail_host'], int(secrets['mail_port']))
    # identify ourselves to smtp gmail client
    s.ehlo()
    # secure our email with tls encryption
    s.starttls()
    # re-identify ourselves as an encrypted connection
    s.ehlo()
    s.login(secrets['mail_login'], secrets['mail_pass'])
    s.send_message(msg)
    s.quit()
    print('Mail sent at {}!'.format(current_date))
