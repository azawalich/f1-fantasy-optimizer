from utils.drivers_api import *
def get_players_streaks(players_data):
    players_streaks = {}
    for single_player in players_data['players']:
        player_name = drivers_api[single_player['id']]
        players_streaks[player_name] = single_player['streak_events_progress']
    return players_streaks