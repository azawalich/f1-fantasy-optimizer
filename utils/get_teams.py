from utils.drivers_api import *
def get_teams(teams_data_dict, current_season, players_prices):
    teams_list = []
    for single_team in teams_data_dict['picked_teams']:
        single_team_list = []
        if current_season is None:
            for single_player in single_team['picked_players']:
                player_name = drivers_api[single_player['player_id']]
                single_team_list.append(player_name)
            teams_list.append(single_team_list)
        else:
            single_team_dict = {
                'team_name': single_team['name'],
                'cash_balance': single_team['cash_balance'],
                'Name': [],
                'Price': [],
                'Purchase_price': []
            }
            for single_player in single_team['picked_players']:
                player_name = drivers_api[single_player['player_id']]
                single_team_dict['Name'].append(player_name)
                single_team_dict['Price'].append(players_prices[player_name])
                single_team_dict['Purchase_price'].append(single_player['purchase_price'])
            teams_list.append(single_team_dict)

    return teams_list