from utils.download_data import *
from utils.save_data import *
import json
import time
from tqdm import tqdm

def get_and_download_current_season_data(single_year):
    players = list(range(0,40))
    print('Downloading current season points data...')
    for single_player in tqdm(players):
        historical_data = download_data('https://fantasy-api.formula1.com/partner_games/f1/players/{}/game_periods_scores?player={}'.format(
            single_player, single_player))
        # save file in json
        if 'errors' not in historical_data.text and "The page you were looking for doesn't exist" not in historical_data.text:
            save_data(
                historical_data, 
                single_path='data/historical/{}/'.format(single_year), 
                data_type='player_{}'.format(single_player), 
                do_print=False
                )
        time.sleep(1)