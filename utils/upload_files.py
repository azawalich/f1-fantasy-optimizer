from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from googleapiclient.http import MediaFileUpload

def get_google_drive_creds():
    SCOPES = ['https://www.googleapis.com/auth/drive']
    # creds = None
    # # The file token.json stores the user's access and refresh tokens, and is
    # # created automatically when the authorization flow completes for the first
    # # time.
    # if os.path.exists('/home/azawalich/datascience/f1-fantasy-optimizer/files/token.json'):
    #     creds = Credentials.from_authorized_user_file('/home/azawalich/datascience/f1-fantasy-optimizer/files/token.json', SCOPES)
        
    # # If there are no (valid) credentials available, let the user log in.
    # if not creds or not creds.valid:
    #     if creds and creds.expired and creds.refresh_token:
    #         creds.refresh(Request())
    #     else:
    #         flow = InstalledAppFlow.from_client_secrets_file(
    #             '/home/azawalich/datascience/f1-fantasy-optimizer/files/credentials2.json', SCOPES)
    #         creds = flow.run_local_server(port=0)
    #     # Save the credentials for the next run
    #     with open('/home/azawalich/datascience/f1-fantasy-optimizer/files/token.json', 'w') as token:
    #         token.write(creds.to_json())
    creds = Credentials.from_authorized_user_file('files/token.json', SCOPES)
    return creds

def create_google_drive_folder(service, creds, folder_name):
    file_metadata = {
    'name': folder_name,
    'mimeType': 'application/vnd.google-apps.folder',
    'parents': ['1gMbUe6SViagGH4Ik20pGZaVW4FyYcnBc']
    }
    file = service.files().create(body=file_metadata, fields='id').execute()
    folder_id = file.get('id')
    print('Folder created: {}, ID: {}'.format(folder_name, folder_id))
    return folder_id

def upload_csv_file_to_google_drive(service, creds, folder_id, file_path, file_name):
    file_metadata = {
        'name': file_name,
        'mimeType': 'application/vnd.google-apps.spreadsheet',
        'parents': [folder_id]
    }
    media = MediaFileUpload(file_path,
                            mimetype='text/csv',
			                chunksize=262144,
                            resumable=True)

    if file_name == 'leagues_results.csv':
        del file_metadata['parents']
        file = service.files().update(
            fileId='1Ydjo1gFkQTLFYaQZXS1Ki3KCnKvVSFLNbqIKl9PjusY',
            body=file_metadata,
            media_body=media).execute()
    
    else:
        file = service.files().create(
            body=file_metadata,
            media_body=media,
            fields='id').execute()
    print('File uploaded {}, ID: {}'.format(file_path, file.get('id')))

def google_drive_upload_wrapper(folder_name_list, file_path_list):
    creds = get_google_drive_creds()
    service = build('drive', 'v3', credentials=creds)
    folder_id = None
    for single_index in range(0, len(folder_name_list)):
        single_folder = folder_name_list[single_index]
        if single_index <= len(file_path_list)-1:
            single_file_path = file_path_list[single_index]
            if single_index == 0:
                folder_id = create_google_drive_folder(service, creds, single_folder)
            file_name = single_file_path.split('/')[-1]
            if file_name == 'leagues_results.csv':
                folder_id = '1gMbUe6SViagGH4Ik20pGZaVW4FyYcnBc'
            if 'data/leagues' in single_file_path:
                file_name = 'leagues'
            upload_csv_file_to_google_drive(service, creds, folder_id, single_file_path, file_name)
