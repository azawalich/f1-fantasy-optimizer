from git import Repo

def git_push(commit_message, files_paths_list):
    repo_path = '.git'
    try:
        repo = Repo(repo_path)
        repo.index.add(files_paths_list)
        repo.index.commit(commit_message)
        origin = repo.remote(name='origin')
        origin.push()
        print('Code pushed')   
    except:
        print('Some error occured while pushing the code')
