import os
import json

def create_folders(folders_list, time, country, session_type):
    time_formatted = time.split('.')[0].replace(' ', '_')
    created_paths = []
    for single_folder in folders_list:
        # define the name of the directory to be created
        if 'leagues' not in single_folder: 
            path = "{}/{}_{}_{}".format(single_folder, time_formatted, country, session_type)
        else:
            path = single_folder
        try:
            os.makedirs(path)
        except OSError:
            print("Creation of the directory {} failed".format(path))
        else:
            print("Successfully created the directory {}".format(path))
        created_paths.append(path)
    return created_paths

def save_data(response_object, data_type, single_path, do_print=True):
    # save file in json
    with open('{}/{}.json'.format(single_path, data_type), 'w', encoding='utf-8') as f:
        json.dump(response_object.text, f, ensure_ascii=False, indent=4)
    
    if do_print:
        print('Saved {} data in {} path.'.format(data_type, single_path))

    return response_object.text