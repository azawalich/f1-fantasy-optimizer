import pandas as pd
import numpy as np
import pickle
import multiprocessing as mp
import json
from tqdm import tqdm
import requests
from bs4 import BeautifulSoup
import time
import re
import utils.banned_drivers as bdr

def create_combinations_df_current(combinations_single_tuple):
    single_combination, data_df, single_team_dict, execution_type = combinations_single_tuple
    data_df['Position'] = pd.to_numeric(data_df['Position'])

    single_combination_players = data_df[data_df['Id'].isin(single_combination)]['Name'].tolist()

    banned_in_team = set(single_combination_players).intersection(set(bdr.banned_drivers))
    temp_dict = None

    # P3 is not here as we want it to be not full - as it is so close to Quali
    if execution_type in ['P1', 'P2', 'Q', 'R', 'SR']:
        banned_in_team = set()

    if len(banned_in_team) == 0:

        players_price_df = data_df[['Id', 'Name', 'Price', 'Position']]
        players_streaks_df = data_df[['Name', 'Quali_streak', 'Race_streak']]

        team_players_dict = { your_key: single_team_dict[your_key] for your_key in ['Name', 'Purchase_price'] }

        temp_df = pd.DataFrame().from_dict(team_players_dict)
        temp_df = pd.merge(
            temp_df,
            data_df,
            on='Name',
            how='left'
        )

        cash_balance = single_team_dict['cash_balance']
        
        leavers = set(temp_df['Name']) - set(single_combination_players)
        leavers_df = temp_df[temp_df['Name'].isin(leavers)]
        selling_purchase_price = leavers_df['Purchase_price'].sum()
        
        newcomers = set(single_combination_players) - set(temp_df['Name'])
        newcomers_df = players_price_df[players_price_df['Name'].isin(newcomers)]
        newcomers_price_sum = newcomers_df['Price'].sum()

        is_possible = int(newcomers_price_sum <= (selling_purchase_price + cash_balance))
        
        if is_possible == 1:
            single_combination_ids = data_df[data_df['Id'].isin(single_combination)]['Id'].tolist()

            constructor = [x for x in single_combination_ids if x[0] == 'C']
            positions_constructor = data_df[data_df['Id'].isin(constructor)]['Position'].sum() * 0.7
            positions_driver = data_df[data_df['Id'].isin([x for x in single_combination_ids if x != constructor])]['Position'].sum() * 0.3

            team_name = single_team_dict['team_name'].replace(' ', '_')
            
            leavers_format = ' '.join(list(leavers))
            changes_needed = len(leavers)

            selling_price = leavers_df['Price'].sum()
            selling_income = selling_price - selling_purchase_price

            stayers = set(temp_df['Name']).intersection(set(single_combination_players))
            stayers_format = ' '.join(list(stayers))
            stayers_df = temp_df[temp_df['Name'].isin(stayers)]
            stayers_df_replaced = temp_df[temp_df['Name'].isin(stayers)].drop(columns='Price').rename(columns={'Purchase_price': 'Price'})

            newcomers_format = ' '.join(list(newcomers))

            cash_to_spend = selling_price + cash_balance

            new_cash_balance = cash_to_spend - newcomers_price_sum

            new_team = pd.concat(
                [
                    stayers_df_replaced[['Name', 'Price']],
                    newcomers_df[['Name', 'Price']]
                ]
            )
            new_team_value = new_team['Price'].sum()
            new_total_budget = new_team_value + new_cash_balance

            players_streaks_df_team = players_streaks_df[players_streaks_df['Name'].isin(single_combination_players)]
            players_streaks_df_team['is_driver'] = 1
            
            players_streaks_df_team.loc[players_streaks_df_team['Name'] == single_combination_players[-1], 'is_driver'] = 0 
            
            players_streaks_df_team['quali_bonus_points_possible'] = 0
            players_streaks_df_team['race_bonus_points_possible'] = 0
            
            for single_player in players_streaks_df_team['Name'].tolist():
                quali_streak = players_streaks_df_team[players_streaks_df_team['Name'] == single_player]['Quali_streak'].tolist()[0]
                race_streak = players_streaks_df_team[players_streaks_df_team['Name'] == single_player]['Race_streak'].tolist()[0]

                is_driver = players_streaks_df_team[players_streaks_df_team['Name'] == single_player]['is_driver'].tolist()[0]
                
                if (is_driver == 1 and quali_streak % 4 == 0 and quali_streak > 0) or (is_driver == 0 and quali_streak % 2 == 0 and quali_streak > 0):
                    players_streaks_df_team.loc[players_streaks_df_team['Name'] == single_player, 'quali_bonus_points_possible'] = 5
                
                if (is_driver == 1 and race_streak % 4 == 0 and race_streak > 0) or (is_driver == 0 and race_streak % 2 == 0 and race_streak > 0):
                    players_streaks_df_team.loc[players_streaks_df_team['Name'] == single_player, 'race_bonus_points_possible'] = 10

            quali_streak_points_sum = players_streaks_df_team['quali_bonus_points_possible'].sum()
            race_streak_points_sum = players_streaks_df_team['race_bonus_points_possible'].sum()

            quali_streak_points_drivers = \
                players_streaks_df_team[(players_streaks_df_team['quali_bonus_points_possible'] > 0) & (players_streaks_df_team['is_driver'] == 1)]['Name'].tolist()
            race_streak_points_drivers = \
                players_streaks_df_team[(players_streaks_df_team['race_bonus_points_possible'] > 0) & (players_streaks_df_team['is_driver'] == 1)]['Name'].tolist()

            quali_streak_points_constructors = \
                players_streaks_df_team[(players_streaks_df_team['quali_bonus_points_possible'] > 0) & (players_streaks_df_team['is_driver'] == 0)]['Name'].tolist()
            race_streak_points_constructors = \
                players_streaks_df_team[(players_streaks_df_team['race_bonus_points_possible'] > 0) & (players_streaks_df_team['is_driver'] == 0)]['Name'].tolist()


            temp_dict = {
                'Previous_team_name': team_name,
                'Is_possible': is_possible,
                'Changes_needed': changes_needed,
                'Selling_income': selling_income,
                'Team': str(single_combination_players),
                'Team_name': ' '.join(single_combination_players),
                'Constructor': single_combination_players[-1],
                'Position_C': positions_constructor,
                'Position_D': positions_driver,
                'Position': round(positions_driver + positions_constructor,3),
                'Race_streak_bonus_points': race_streak_points_sum,
                'Quali_streak_bonus_points': quali_streak_points_sum,
                'Race_streak_drivers': ' '.join(race_streak_points_drivers),
                'Race_streak_constructors': ' '.join(race_streak_points_constructors),
                'Quali_streak_drivers': ' '.join(quali_streak_points_drivers),
                'Quali_streak_constructors': ' '.join(quali_streak_points_constructors),
                'Leavers': leavers_format,
                'Stayers': stayers_format,
                'Newcomers': newcomers_format,
                'Cash_to_spend': cash_to_spend,
                'New_cash_balance': new_cash_balance,
                'New_team_value': new_team_value,
                'New_total_budget': new_total_budget,
                'Selling_price': selling_price,
                'Selling_purchase_price': selling_purchase_price,
                'Previous_team': str(temp_df['Name'].tolist()),
                'Previous_team_str': ' '.join(temp_df['Name'])
            }

    return temp_dict

def create_combinations_df(combinations_single_tuple):
    single_combination, data_df, teams_list = combinations_single_tuple
    team_1_set, team_2_set, team_3_set = [set(ele) for ele in teams_list]
    is_current_team = '0'

    players_price_df = data_df[['Id', 'Name', 'Price']]
    
    temp_df = data_df[data_df['Id'].isin(single_combination)]
    constructor = [x for x in temp_df['Id'].tolist() if x[0] == 'C']
    positions_constructor = temp_df[temp_df['Id'].isin(constructor)]['Position'].sum() * 0.7
    positions_driver = temp_df[~temp_df['Id'].isin(constructor)]['Position'].sum() * 0.3
    combination_team_list = temp_df['Name'].tolist()
    combination_team_list_set = set(combination_team_list)

    players_to_add_1 = list(combination_team_list_set - team_1_set)
    players_to_add_2 = list(combination_team_list_set - team_2_set)
    players_to_add_3 = list(combination_team_list_set - team_3_set)

    players_to_remove_1 = list(team_1_set - combination_team_list_set)
    players_to_remove_2 = list(team_2_set - combination_team_list_set)
    players_to_remove_3 = list(team_3_set - combination_team_list_set)

    players_to_remove_prices_1 = { your_key: players_price_df[players_price_df['Name'] == your_key]['Price'].tolist()[0] for your_key in players_to_remove_1}
    players_to_remove_prices_2 = { your_key: players_price_df[players_price_df['Name'] == your_key]['Price'].tolist()[0] for your_key in players_to_remove_2}
    players_to_remove_prices_3 = { your_key: players_price_df[players_price_df['Name'] == your_key]['Price'].tolist()[0] for your_key in players_to_remove_3}

    players_to_remove_prices_sum_1 = sum(players_to_remove_prices_1.values())
    players_to_remove_prices_sum_2 = sum(players_to_remove_prices_2.values())
    players_to_remove_prices_sum_3 = sum(players_to_remove_prices_3.values())

    players_change_income_1 = players_to_remove_prices_sum_1
    players_change_income_2 = players_to_remove_prices_sum_2
    players_change_income_3 = players_to_remove_prices_sum_3

    changes_needed_1 = len(players_to_add_1)
    changes_needed_2 = len(players_to_add_2)
    changes_needed_3 = len(players_to_add_3)

    if changes_needed_1 == 0:
        is_current_team = 'Team 1'

    if changes_needed_2 == 0:
        is_current_team = 'Team 2'
    
    if changes_needed_3 == 0:
        is_current_team = 'Team 3'

    temp_dict = {
        'Team': str(combination_team_list),
        'Team_name': ' '.join(combination_team_list),
        'Constructor': combination_team_list[-1],
        'Price': round(float(temp_df['Price'].sum()),3),
        'Position_C': positions_constructor,
        'Position_D': positions_driver,
        'Position': round(positions_driver + positions_constructor,3),
        'Current_Team': is_current_team,
        'Changes_Team_1': changes_needed_1,
        'Changes_Income_Team_1': players_change_income_1,
        'Changes_Team_2': changes_needed_2,
        'Changes_Income_Team_2': players_change_income_2,
        'Changes_Team_3': changes_needed_3,
        'Changes_Income_Team_3': players_change_income_3,
    }
    return temp_dict

def make_combinations_df_wrapper(combinations_tuple, current_season):
    print('Creating permutations...')
    if current_season is None:
        function_to_run = create_combinations_df
    else:
        function_to_run = create_combinations_df_current
    
    with mp.Pool(mp.cpu_count()) as p:
        list_of_rows = list(
            tqdm(
                p.imap(function_to_run, combinations_tuple), 
                total=len(combinations_tuple)
            )
        )

    list_of_rows = [i for i in list_of_rows if i]

    print('Joining permutations to dataframe...')
    combinations_df_list = pd.DataFrame(list_of_rows)
    return combinations_df_list

def get_json_prices(folder_path):
    drivers = {
        'Name': [],
        'Price': []
    }

    constructors = {
        'Name': [],
        'Price': []
    }

    with open('{}/players.json'.format(folder_path)) as json_file:
        data = json.load(json_file)
    data_dict = json.loads(data)

    for single_player in data_dict['players']:
        if single_player['last_name'] == '':
            constructors['Name'].append(single_player['display_name'])
            constructors['Price'].append(single_player['price'])
        else:
            drivers['Name'].append(single_player['last_name'])
            drivers['Price'].append(single_player['price'])

    drivers_df = pd.DataFrame().from_dict(drivers)
    constructors_df = pd.DataFrame().from_dict(constructors)
    drivers_constructors_df = drivers_df.append(constructors_df)
    return drivers_constructors_df

def replace_original_prices(drivers_constructors_df):
    drivers = pd.read_csv('files/drivers.csv')
    constructors = pd.read_csv('files/constructors.csv')

    data_df = drivers.append(constructors).drop(columns='Price')
    data_df = pd.merge(data_df, drivers_constructors_df, how='left', on='Name')
    data_df = data_df[['Id', 'Name', 'Price', 'Position']]

    return data_df

def replace_original_positions(folder_path, prices_df):
    endpoints_ranking = [
        'https://www.formula1.com/en/results.html/2021/drivers.html',
        'https://www.formula1.com/en/results.html/2021/team.html'
    ]

    ranking_dict = {
        'drivers': {
            'filepath': 'files/drivers.csv',
            'data': {
                'Name': [],
                'Position_championship': []
                }
            },
        'constructors': {
            'filepath': 'files/constructors.csv',
            'data': {
                'Name': [],
                'Position_championship': []
                }
            },
    }

    for single_endpoint in endpoints_ranking:
        response = requests.get(single_endpoint)
        soup = BeautifulSoup(response.text, 'lxml')
        table = soup.find_all('table')[0] # Grab the first table
        if 'drivers' in single_endpoint:
            surnames = table.find_all('span', {'class': 'hide-for-mobile'})
        elif 'team' in single_endpoint:
            surnames = table.find_all('a')
        
        for single_surname in surnames:
            name = single_surname.getText()
            name_subbed = re.sub('RÃ¤ikkÃ¶nen', 'Raikkonen', name)
            name_subbed_final = re.sub(' Racing| Honda| Mercedes| Renault| Ferrari', '', name_subbed)
            if 'drivers' in single_endpoint:
                ranking_dict['drivers']['data']['Name'].append(name_subbed_final)

            elif 'team' in single_endpoint:
                ranking_dict['constructors']['data']['Name'].append(name_subbed_final)
        
        positions = table.find_all('td', {'class': 'dark'})
        for single_position in positions:
            if 'drivers' in single_endpoint:
                if positions.index(single_position) % 3 == 2:
                    ranking_dict['drivers']['data']['Position_championship'].append(single_position.getText())
            elif 'team' in single_endpoint:
                if positions.index(single_position) % 2 == 1:
                    ranking_dict['constructors']['data']['Position_championship'].append(single_position.getText())
        time.sleep(1)

    final_df = pd.DataFrame()
    for single_key in ranking_dict.keys():
        desired_df = pd.DataFrame.from_dict(ranking_dict[single_key]['data'])
        original_df = prices_df[prices_df['Name'].isin(desired_df['Name'])]
        desired_df_empty_names = desired_df[desired_df['Position_championship'] == '0']['Name'].tolist()

        desired_df_empty = original_df[original_df['Name'].isin(desired_df_empty_names)]
        desired_df_full = desired_df[~desired_df['Name'].isin(desired_df_empty_names)]

        desired_df_full_joined = pd.merge(original_df, desired_df_full, on='Name', how='left').drop(columns='Position').rename(
            columns={'Position_championship': 'Position'})
        desired_df_full_joined = desired_df_full_joined.append(desired_df_empty)

        desired_df_full_joined_final = desired_df_full_joined.dropna()
        desired_df_full_joined_final.to_csv(ranking_dict[single_key]['filepath'])

        final_df = final_df.append(desired_df_full_joined_final)
    
    return final_df


def replace_original_prices_wrapper(folder_path, execution_type):
    new_prices_df = get_json_prices(folder_path)
    final_df = replace_original_prices(new_prices_df)
    if execution_type == 'R':
        final_df = replace_original_positions(folder_path, final_df)
        print('Positions based on championship changed successfully...')
    return final_df

def convert_strikes_to_df(streaks_dict):
    players_streaks_dict_unpacked = {
        'Name': [],
        'Quali_streak': [],
        'Race_streak': []
    }

    for single_player in streaks_dict.keys():
        players_streaks_dict_unpacked['Name'].append(single_player)
        players_streaks_dict_unpacked['Quali_streak'].append(
            int(streaks_dict[single_player]['top_ten_in_a_row_qualifying_progress'])
            )
        players_streaks_dict_unpacked['Race_streak'].append(
            int(streaks_dict[single_player]['top_ten_in_a_row_race_progress'])
            )

    players_streaks_df = pd.DataFrame().from_dict(players_streaks_dict_unpacked)
    return players_streaks_df

def create_combinations_full_df(folder_path, current_folder, teams_list, players_prices_dict, players_streaks_dict, current_season, type_of_execution):
    
    data_df = replace_original_prices_wrapper(folder_path, type_of_execution)
    players_streaks_df = convert_strikes_to_df(players_streaks_dict)
    
    data_df_joined = pd.merge(data_df, players_streaks_df, on='Name', how='left')

    file_path = '{}/drivers_constructors.csv'.format(current_folder)
    data_df_joined.to_csv(file_path)
    print('Saved Drivers/Constructors CSV in {} path.'.format(folder_path))

    with open('files/combinations.pickle', 'rb') as handle:
        combinations_with_constructors = pickle.load(handle)
    print('Loaded permutations data.')

    if current_season is None:
        combinations_tuple = list(
            zip(
                combinations_with_constructors,
                [data_df_joined] * len(combinations_with_constructors),
                [teams_list] * len(combinations_with_constructors)
            )
        )

        combinations_df = make_combinations_df_wrapper(combinations_tuple, current_season)
        combinations_df = [combinations_df]
    else:
        combinations_df = []
        for single_team in teams_list:
            combinations_tuple = list(
                zip(
                    combinations_with_constructors,
                    [data_df_joined] * len(combinations_with_constructors),
                    [single_team] * len(combinations_with_constructors),
                    [type_of_execution] * len(combinations_with_constructors)
                )
            )

            combinations_df_temp = make_combinations_df_wrapper(combinations_tuple, current_season)
            combinations_df.append(combinations_df_temp)

    return combinations_df, file_path

def count_place_diff(df, file_name, old_folder_path, budget):
    if isinstance(budget, str):
        columns_to_pick = ['Team', 'Place']
        columns_rename_dict = {'Place': 'Place_old', 'Team': 'Team_name'}
        merge_column = 'Team_name'
    else:
        columns_to_pick = ['Team', 'Place']
        columns_rename_dict = {'Place': 'Place_old'}
        merge_column = 'Team'
    
    old_combinations_df = pd.read_csv(
        '{}/{}'.format(old_folder_path, file_name)
        )[columns_to_pick].rename(columns=columns_rename_dict)

    df = pd.merge(df, old_combinations_df, how='left', on=merge_column)
    df['Place_diff'] = -1*(df['Place'] - df['Place_old'])
    df = df.drop(columns='Place_old').fillna('Newcomer!')
    return df

def sort_and_save_combinations_full_df(combinations_full_df, folder_path, previous_folder_path, budget, execution_type):
    folder_name = folder_path.split('/')[-1]
    folder_names = [folder_name, folder_name]
    files_paths = []
    
    if isinstance(budget, str):
        sort_list = ['Race_streak_bonus_points', 'Quali_streak_bonus_points', 'Position', 'Selling_income', 'New_total_budget']
        name_add = '{}_'.format(budget)
    else:
        sort_list = ['Position','Price']
        name_add = ''
    #sort and save all combinations to file
    combinations_df_sorted = combinations_full_df.sort_values(sort_list, ascending = False)
    combinations_df_sorted['Place'] = np.arange(len(combinations_df_sorted))+1

    # combinations_df_sorted = count_place_diff(
    #     combinations_df_sorted,
    #     '{}combinations.csv'.format(name_add),
    #     previous_folder_path,
    #     budget
    # )

    # file_path = '{}/{}combinations.csv'.format(folder_path, name_add)
    # combinations_df_sorted.drop(columns='Team').rename(columns={'Team_name': 'Team'}).to_csv(file_path)
    # print('Saved ALL combinations data in {} path.'.format(folder_path))
    # files_paths.append(file_path)

    #filter drivers/teams with small chance for top10/top5 finish as well as budget
    #save these combinations to another file

    # P3 is not here as we want it to be not full - as it is so close to Quali
    if execution_type not in ['P1', 'P2', 'Q', 'R', 'SR']:
        for single_banned_driver in bdr.banned_drivers:
            if bdr.banned_drivers.index(single_banned_driver) == 0:
                combinations_df_desired = combinations_df_sorted[
                    ~combinations_df_sorted['Team_name'].str.contains(single_banned_driver)
                    ]
            else:
                combinations_df_desired = combinations_df_desired[
                    ~combinations_df_desired['Team_name'].str.contains(single_banned_driver)
                    ]
    else:
        combinations_df_desired = combinations_df_sorted

    if isinstance(budget, str):
        combinations_df_desired = combinations_df_desired[combinations_df_desired['Is_possible'] == 1]
    else:
        combinations_df_desired = combinations_df_desired[combinations_df_desired['Price'] <= budget]

    combinations_df_desired = count_place_diff(
        combinations_df_desired,
        '{}combinations_desired.csv'.format(name_add),
        previous_folder_path,
        budget
    )
    file_path = '{}/{}combinations_desired.csv'.format(folder_path, name_add)
    combinations_df_desired.drop(columns='Team').rename(columns={'Team_name': 'Team'}).to_csv(file_path)
    print('Saved DESIRED combinations data in {} path.'.format(folder_path))
    files_paths.append(file_path)
    return folder_names, files_paths
