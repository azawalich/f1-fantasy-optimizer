import datetime
import pandas as pd
import os
import utils.send_email as se
import argparse
import glob
from utils.python_processes_running import *

current_date = datetime.datetime.now().strftime("%d-%m-%Y %H:%M")

parser = argparse.ArgumentParser()
parser.add_argument("--exceptional", help="If exceptional run", type=str)
parser.add_argument("--names", help="Names for email", type=str)

args = parser.parse_args()
if not args.exceptional:
    executions = pd.read_csv('files/execution.csv')
    executions_filtered = executions[executions['Cancelled'] == 0]
    executions_head = executions_filtered[executions_filtered['Executed'] == 0].head(1)
    executions_head_time = executions_head['Execution Datetime'].tolist()[0]
    executions_desired = executions_filtered[executions_filtered['Execution Datetime'] == current_date]
    if executions_desired.shape[0] > 0:
        execute_script=True
        reason = 'Reason: Scheduled Execution'
        executions.loc[executions['Execution Datetime'] == current_date, 'Executed'] = 1
        execution_name = executions_desired['Name'].tolist()[0]
        execution_type = executions_desired['Type'].tolist()[0]
        # executions.to_csv('files/execution_test.csv')
        executions = executions.loc[:,~executions.columns.str.contains('^unnamed', case=False)] 
        executions.to_csv('files/execution.csv')
    else:
        time_delta = datetime.datetime.strptime(current_date, "%d-%m-%Y %H:%M") - datetime.datetime.strptime(executions_head_time, "%d-%m-%Y %H:%M")
        
        minutes_after = int(round((time_delta.days*24*60 + round(time_delta.seconds/60,0)),0))
        python_wrapper_running = python_processes_running(process_text = 'python3 wrapper.py')

        if minutes_after <= 60 and minutes_after > 0 and python_wrapper_running == False:
            execute_script=True
            reason = 'Reason: Late Scheduled Execution'
            executions.loc[executions['Execution Datetime'] == executions_head_time, 'Executed'] = 1
            execution_name = executions_head['Name'].tolist()[0]
            execution_type = executions_head['Type'].tolist()[0]
            executions.to_csv('files/execution.csv')
        else:
            reason = 'Error Reason!'
            execute_script=False
        
else:
    execute_script=True
    reason = 'Reason: Pricing Change Execution - {}'.format(args.names)

    optimizer_last_file = sorted(glob.glob('data/raw/*'))[-1]
    optimizer_last_file_splitted = optimizer_last_file.split('_')
    
    execution_name = optimizer_last_file_splitted[-2]

    if optimizer_last_file_splitted[-1] in ['P1', 'P2', 'P3', 'Q', 'R', 'SR']:
        execution_type = 'A{}'.format(optimizer_last_file_splitted[-1])
    else:
        execution_type = optimizer_last_file_splitted[-1]

if execute_script:
    se.send_email(
        subject = 'Starting F1 Fantasy data download...',
        content = 'Starting F1 Fantasy data download for {} in {} Race Week. {}'.format(
            execution_name,
            execution_type,
            reason
        )
    )

    budget=100.0
    os.system('python3 wrapper.py --country {} --type {} --budget {} --commit True --current_season True'.format(execution_name, execution_type, budget))

    se.send_email(
        subject = 'Ended F1 Fantasy data download...',
        content = 'Ended F1 Fantasy data download for {} in {} Race Week. {}'.format(
            execution_name,
            execution_type,
            reason
        )
    )


