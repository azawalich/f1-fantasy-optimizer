#! /bin/bash
cd ~/f1-fantasy-optimizer
# source variables
. ./secrets.sh
COOKIE_FILENAME=cookie.py
/usr/bin/python3 cookie_wrapper.py --f1-fantasy-user=$mail_login2 --f1-fantasy-pass=$f1_fantasy_pass --cookie-filename=$COOKIE_FILENAME
# copy cookie to dashboard folder
cp utils/$COOKIE_FILENAME ../f1-fantasy-dashboard/utils/$COOKIE_FILENAME