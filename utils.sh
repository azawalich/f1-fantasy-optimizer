# permissions for python file
chmod 755 execute.py
chmod 755 wrapper.py
chmod 755 runner.sh
chmod 755 cookie_runner.sh

# crontab content
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
* * * * * ~/f1-fantasy-optimizer/runner.sh
0 8 * * 3 ~/f1-fantasy-optimizer/cookie_runner.sh 