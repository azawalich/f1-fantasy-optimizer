from utils.generate_cookie import *
import utils.send_email as se
import argparse 

parser = argparse.ArgumentParser()
parser.add_argument("--f1-fantasy-user", help="F1 Fantasy user", type=str)
parser.add_argument("--f1-fantasy-pass", help="F1 Fantasy pass", type=str)
parser.add_argument("--cookie-filename", help="Filename to save cookie to", type=str)
args = parser.parse_args()

cookie = generate_cookie(args.f1_fantasy_user, args.f1_fantasy_pass)

if cookie != None:
    with open("utils/{}".format(args.cookie_filename), "w") as text_file:
        text_file.write("cookie_hash = '{}'".format(cookie))

    se.send_email(
        subject = 'Cookie update succeeded!',
        content = 'Cookie update succeeded! Mail from wrapper - cookie type: {}, cookie content: {}'.format(
            type(cookie), str(cookie)
        )
    )
else:
    se.send_email(
        subject = 'Cookie update failed!',
        content = 'Cookie update failed! Mail from wrapper - cookie type: {}, cookie content: {}'.format(
            type(cookie), str(cookie)
        )
    )